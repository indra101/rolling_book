<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Level;
use App\Models\SubLevel;
use App\Models\ActiveLevel;
use App\Models\Ortu;
use App\Models\Siswa;
use Auth;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('*', function ($view) {

            if(Auth::user()) {
                $id_role = Auth::user()->id_role;
            } else {
                $id_role = 1;
            }

            $siswas_ortu = null;

            if($id_role == 1 || $id_role == 2) {
                $level_sidebar = Level::whereIn('id', ActiveLevel::select('id_level')->get())->get();
            } else { // Ortu
                $level_sidebar = Level::whereIn('id', SubLevel::select('id_level')->whereIn('id', Siswa::select('id_sub_level')->where('id_ortu', Auth::user()->id_ortu)->get())->get())
                                        ->whereIn('id', ActiveLevel::select('id_level')->get())
                                        ->get();
                
                $siswas_ortu = Siswa::where('id_ortu', Auth::user()->id_ortu)->get();
            }

            

            $view->with('level_sidebar', $level_sidebar);
            $view->with('siswas_ortu', $siswas_ortu);

        });
    }
}
