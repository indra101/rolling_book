<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Buku;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\SubLevel;
use App\Models\User;
use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\Ortu;
use App\Models\Role;

use Spatie\PdfToImage\Pdf;
use Storage;
use File;
use Image;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::find(Auth::user()->id);
        $user->nama_role = Role::find($user->id_role)->name;

        return view('profile', ['menu' => 'profile', 'user' => $user]);
    }

    public function edit_profil(Request $request)
    {
        $this->validate($request, [
            //'nama' => 'required|string|max:255',
            //'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => 'nullable|string|min:8|confirmed',
            'alamat' => 'required|string|max:255',
            'file_upload' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG',
        ]);

        $id = Auth::user()->id;
        $user = User::find($id);
        //$user->name = $request->nama;
        //$user->email = $request->email;
        $user->hp = $request->hp;
        $user->alamat = $request->alamat;

        if(!empty($request->password))
            $user->password = bcrypt($request->password);

        //$file_upload = $request->file('file_upload');

        $data = $request->image;

        if(!empty($data)) {

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $file_upload = base64_decode($data);

            $nama_file = $this->uploadFile($file_upload, $id);

            if(!$nama_file) {
                return redirect()->route('profile')
                            ->with('danger','Gagal upload foto, mohon ulangi lagi!');
            }

            $user->foto = $nama_file;
        }
        
        $user->save();
        
        return redirect()->route('profile')
                        ->with('success','Berhasil mengubah profil!');
    }

    function uploadFile($file, $id_user) {

        //$name = $file->getClientOriginalName();
        $name = time().'.png';
        //$path = $file->storeAs('public/files', $name);

        //$url = Storage::url($path);
        $destinationPath = public_path()."/images/profil/";

        if(!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true, true);
        }

        if(!File::exists($destinationPath.'/thumb/')) {
            File::makeDirectory($destinationPath.'/thumb/', 0755, true, true);
        }

        try {

            $rnd = rand(10, 1000);
            $image_name = "$id_user-$rnd.jpg";

            $path = $destinationPath.$image_name;
            file_put_contents($path, $file);

            $img = Image::make($path);
            $img->resize(40, 40, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'thumb/'.$image_name);

            // Storage::delete($path);

        } catch (Exception $e) {
            return false;    
        }

        return $image_name;
    }

}
