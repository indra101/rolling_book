<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Kontak;

use Storage;
use File;
use Image;
use Auth;

class KontakController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $kontaks = Kontak::all();

        return view('kontak.kontak', ['menu' => 'kontak', 'kontaks' => $kontaks]);
    }

    public function add_kontak(Request $request)
    {
        $id_role = Auth::user()->id_role;

        if($id_role != 1 && $id_role != 2) {
            return redirect()->route('home')
                        ->with('danger','Anda tidak memiliki akses!');
        }

        $this->validate($request, [
            'nama' => 'required',
            'title' => 'required',
            'hp' => 'required',
            //'file_upload' => 'required|mimes:jpg,JPG,jpeg,JPEG,png,PNG',
            'image' => 'required',
        ]);

        $kontak = new Kontak;
        $kontak->nama = $request->nama;
        $kontak->title = $request->title;
        $kontak->hp = $request->hp;
        $kontak->save();

        //$file_data = array();
        //$file_upload = $request->file('file_upload');

        $data = $request->image;

        if(!empty($data)) {

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $file_upload = base64_decode($data);
            $nama_file = $this->uploadFile($file_upload, $kontak->id);

            if(!$nama_file) {
                $kontak->delete();

                return redirect()->route('kontak')
                            ->with('danger','Gagal upload foto, mohon ulangi lagi!');    
            }

            $kontak->foto = $nama_file;
        }

        // if(!empty($file_upload)) {
        //     $nama_file = $this->uploadFile($file_upload, $kontak->id);
        // }

        // if(!$nama_file) {
        //     $kontak->delete();

        //     return redirect()->route('kontak')
        //                 ->with('danger','Gagal menambah kontak, mohon ulangi lagi!');    
        // }

        $kontak->foto = $nama_file;
        $kontak->save();

        return redirect()->route('kontak')
                        ->with('success','Berhasil menambah kontak!');
    }

    public function get_kontak_edit(Request $request)
    {
        $id = $request->id;
        $kontak = Kontak::find($id);

        return $kontak->toJson();
    }

    public function edit_kontak(Request $request)
    {
        $id_role = Auth::user()->id_role;

        if($id_role != 1 && $id_role != 2) {
            return redirect()->route('home')
                        ->with('danger','Anda tidak memiliki akses!');
        }

        $this->validate($request, [
            'nama_edit' => 'required',
            'title_edit' => 'required',
            'hp_edit' => 'required',
            //'file_upload' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG',
        ]);

        $kontak = Kontak::find($request->id_kontak);
        $kontak->nama = $request->nama_edit;
        $kontak->title = $request->title_edit;
        $kontak->hp = $request->hp_edit;
        //$file_upload = $request->file('file_upload_edit');

        $data = $request->image;

        // echo $data;
        // die;

        if(!empty($data)) {

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $file_upload = base64_decode($data);
            $nama_file = $this->uploadFile($file_upload, $kontak->id);

            if(!$nama_file) {
                return redirect()->route('kontak')
                            ->with('danger','Gagal upload foto, mohon ulangi lagi!');    
            }

            $kontak->foto = $nama_file;
        }
        
        $kontak->save();

        return redirect()->route('kontak')
                        ->with('success','Berhasil mengubah kontak!');
    }

    public function delete_kontak(Request $request)
    {
        $id_role = Auth::user()->id_role;

        if($id_role != 1 && $id_role != 2) {
            return redirect()->route('home')
                        ->with('danger','Anda tidak memiliki akses!');
        }
        
        $kontak = Kontak::find($request->id);
        $kontak->delete();

        return redirect()->route('kontak')
                        ->with('success','Berhasil menghapus kontak!');
    }

    function uploadFile($file, $id_kontak) {

        // $name = $file->getClientOriginalName();
        // $path = $file->storeAs('public/files', $name);
        // $url = Storage::url($path);

        $name = time().'.png';

        $destinationPath = public_path()."/images/kontak/";

        if(!File::exists($destinationPath)) {
            File::makeDirectory($destinationPath, 0755, true, true);
        }

        try {

            $rnd = rand(10, 1000);
            $image_name = "$id_kontak-$rnd.jpg";

            $path = $destinationPath.$image_name;
            file_put_contents($path, $file);

            // $img = Image::make(public_path().$url);
            // $img->resize(200, 200, function ($constraint) {
            //     $constraint->aspectRatio();
            // })->save($destinationPath.$image_name);

            // Storage::delete($path);

        } catch (Exception $e) {
            return false;    
        }

        return $image_name;
    }

}
