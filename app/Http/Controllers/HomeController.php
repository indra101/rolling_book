<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RiwayatBaca;
use App\Models\Siswa;
use App\Models\SubLevel;
use App\Models\Level;
use App\Models\ActiveLevel;
use App\Models\Setting;
use App\Models\TahunAjar;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $report = Setting::where('nama', 'report')->first()->value;
        $id_tahun = Setting::where('nama', 'tahun_ajar')->first()->value;
        $tahun_ajar = TahunAjar::find($id_tahun);

        $siswas = Siswa::where('id_ortu', Auth::user()->id_ortu)->whereIn('id_sub_level', SubLevel::select('id')->whereIn('id_level', ActiveLevel::select('id_level')->get())->get())->get();

        foreach($siswas as $siswa) {
            $siswa->jml_baca = RiwayatBaca::where('id_siswa', $siswa->id)->where('tahun_ajar', $id_tahun)->count();
            $siswa->nama_sub_level = $siswa->sub_level->nama;
            $siswa->nama_level = $siswa->sub_level->level->nama;
        }
        
        $tahuns = TahunAjar::groupBy('tahun')->orderBy('tahun', 'desc')->pluck('tahun', 'tahun');

        return view('home', ['menu' => 'home', 'siswas' => $siswas, 'report' => $report, 'tahun_ajar' => $tahun_ajar, 'tahuns' => $tahuns]);
    }

    public function kontak()
    {
        return view('kontak', ['menu' => 'kontak']);
    }

    public function set_report(Request $request)
    {
        $val = $request->val;

        $setting = Setting::where('nama', 'report')->first();
        $setting->value = $val;
        $setting->save();

        $pesan = ($val == 1) ? 'enable' : 'disable';

        return redirect()->route('home')
                        ->with('success','Berhasil '.$pesan.' report!');
    }

    public function set_tahun_ajar(Request $request)
    {
        $tahun = $request->tahun_ajar;
        $semester = $request->semester;

        $tahun_ajar = TahunAjar::where('tahun', $tahun)->where('semester', $semester)->first();

        if($tahun_ajar) {
            $setting = Setting::where('nama', 'tahun_ajar')->first();
            $setting->value = $tahun_ajar->id;
            $setting->save();

            $tipe = 'success';
            $pesan = 'Berhasil mengubah Tahun Ajaran!';

        } else {
            $tipe = 'danger';
            $pesan = 'Gagal mengubah Tahun Ajaran / Belum ada data!';
        }

        return redirect()->route('home')
                        ->with($tipe, $pesan);
    }
}
