<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubLevel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_levels';

    protected $fillable = [
        'nama', 'id_level', 'created_by'
    ];

    public function level(){
        return $this->belongsTo('App\Models\Level','id_level');
    }

    public function bukus() {
        return $this->hasMany('App\Models\Buku', 'id_sub_level');
    }

    public function siswas() {
        return $this->hasMany('App\Models\Siswa', 'sub_level');
    }
}
