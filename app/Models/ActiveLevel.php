<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActiveLevel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'active_levels';

    protected $fillable = [
        'id_level'
    ];

    public function level(){
        return $this->belongsTo('App\Models\Level','id_level');
    }

}
