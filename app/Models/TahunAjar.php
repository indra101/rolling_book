<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TahunAjar extends Model
{
    protected $fillable = [
        'tahun', 'semester'
    ];

}
