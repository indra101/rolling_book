<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = [
        'nama', 'kode', 'created_by'
    ];

    public function sub_levels() {
        return $this->hasMany('App\Models\SubLevel', 'id_level');
    }

}
