<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $fillable = [
        'id_ortu', 'id_sub_level', 'nama', 'panggilan'
    ];

    public function sub_level(){
        return $this->belongsTo('App\Models\SubLevel','id_sub_level');
    }

    public function ortu(){
        return $this->belongsTo('App\Models\Ortu','id_ortu');
    }

    public function riwayat_bacas() {
        return $this->hasMany('App\Models\RiwayatBaca', 'id_siswa');
    }
}
