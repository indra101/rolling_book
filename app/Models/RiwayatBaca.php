<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RiwayatBaca extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'riwayat_baca';

    protected $fillable = [
        'id_siswa', 'id_buku', 'tgl_baca', 'created_at', 'updated_at'
    ];

    public function siswa(){
        return $this->belongsTo('App\Models\Siswa','id_siswa');
    }

    public function buku(){
        return $this->belongsTo('App\Models\Buku','id_buku');
    }
}
