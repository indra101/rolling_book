<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ortu extends Model
{
    protected $fillable = [
        'nama', 'email'
    ];

    public function siswa(){
        return $this->hasMany('App\Models\Siswa', 'id_ortu');
    }

}
