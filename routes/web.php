<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth/login');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/set_report', 'HomeController@set_report')->name('set_report');
Route::post('/set_tahun_ajar', 'HomeController@set_tahun_ajar')->name('set_tahun_ajar');

Route::get('/kontak', 'KontakController@index')->name('kontak');
Route::post('/add_kontak', 'KontakController@add_kontak')->name('add_kontak');
Route::post('/edit_kontak', 'KontakController@edit_kontak')->name('edit_kontak');
Route::post('/delete_kontak', 'KontakController@delete_kontak')->name('delete_kontak');
Route::get('/get_kontak_edit', 'KontakController@get_kontak_edit')->name('get_kontak_edit');

Route::get('/read/{id}', ['as'=>'read','uses'=>'BukuController@read']);

Route::get('/katalog', 'BukuController@katalog')->name('katalog');
Route::get('/list_book', 'BukuController@list_book')->name('list_book');
Route::get('/get_bukus', 'BukuController@get_bukus')->name('get_bukus');
Route::post('/add_buku', 'BukuController@add_buku')->name('add_buku');
Route::get('/convert', 'BukuController@convert')->name('convert');
Route::get('/katalog_level/{id}', 'BukuController@katalog_level')->name('katalog_level');
Route::get('/get_sub_levels_buku', 'BukuController@get_sub_levels_buku')->name('get_sub_levels_buku');
Route::get('/get_buku_edit', 'BukuController@get_buku_edit')->name('get_buku_edit');
Route::post('/edit_buku', 'BukuController@edit_buku')->name('edit_buku');
Route::post('/delete_buku', 'BukuController@delete_buku')->name('delete_buku');
Route::get('/set_status_buku', 'BukuController@set_status_buku')->name('set_status_buku');

Route::get('/laporan/{id}', 'RiwayatController@laporan')->name('laporan');
Route::post('/add_riwayat', 'RiwayatController@add_riwayat')->name('add_riwayat');
Route::get('/get_riwayats', 'RiwayatController@get_riwayats')->name('get_riwayats');
Route::get('/riwayat_baca', 'RiwayatController@riwayat_baca')->name('riwayat_baca');
Route::get('/edit_riwayat_page/{id}', 'RiwayatController@edit_riwayat_page')->name('edit_riwayat_page');
Route::post('/edit_riwayat', 'RiwayatController@edit_riwayat')->name('edit_riwayat');
Route::post('/delete_riwayat', 'RiwayatController@delete_riwayat')->name('delete_riwayat');

Route::get('/level', 'LevelController@index')->name('level');
Route::get('/get_levels', 'LevelController@get_levels')->name('get_levels');
Route::get('/set_status', 'LevelController@set_status')->name('set_status');

Route::get('/sub_level/{id}', 'SubLevelController@index')->name('sub_level');
Route::get('/get_sub_levels', 'SubLevelController@get_sub_levels')->name('get_sub_levels');
Route::post('/add_sub_level', 'SubLevelController@add_sub_level')->name('add_sub_level');
Route::post('/delete_sub_level', 'SubLevelController@delete')->name('delete_sub_level');
Route::post('/edit_sub_level', 'SubLevelController@edit_sub_level')->name('edit_sub_level');

Route::get('/siswa', 'SiswaController@index')->name('siswa');
Route::get('/get_siswas', 'SiswaController@get_siswas')->name('get_siswas');
Route::post('/add_siswa', 'SiswaController@add_siswa')->name('add_siswa');
Route::get('/get_siswa_edit', 'SiswaController@get_siswa_edit')->name('get_siswa_edit');
Route::post('/edit_siswa', 'SiswaController@edit_siswa')->name('edit_siswa');
Route::post('/delete_siswa', 'SiswaController@delete_siswa')->name('delete_siswa');

Route::get('/ortu', 'OrtuController@index')->name('ortu');
Route::get('/get_ortus', 'OrtuController@get_ortus')->name('get_ortus');
Route::post('/add_ortu', 'OrtuController@add_ortu')->name('add_ortu');
Route::get('/get_ortu_edit', 'OrtuController@get_ortu_edit')->name('get_ortu_edit');
Route::post('/edit_ortu', 'OrtuController@edit_ortu')->name('edit_ortu');
Route::post('/delete_ortu', 'OrtuController@delete_ortu')->name('delete_ortu');
Route::post('/reset_pass', 'OrtuController@reset_pass')->name('reset_pass');
Route::get('/get_ortu_view', 'OrtuController@get_ortu_view')->name('get_ortu_view');

Route::get('/report', 'ReportController@index')->name('report');
Route::get('/get_reports', 'ReportController@get_reports')->name('get_reports');
// Route::get('/get-excelItem', ['as'=>'get.excelItem','uses'=>'ReportController@getBladeExcel']);

Route::post('/uploadFolder', 'BukuController@uploadFolder')->name('uploadFolder');

Route::get('/profile', 'UserController@index')->name('profile');
Route::post('/edit_profil', 'UserController@edit_profil')->name('edit_profil');