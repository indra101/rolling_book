@extends('layouts.app')

@section('content')

<style>

.card-profile .card-avatar {
    max-width: 230px;
    max-height: 230px;
}

</style>

<script src="https://demo.itsolutionstuff.com/plugin/croppie.js"></script>
<link rel="stylesheet" href="https://demo.itsolutionstuff.com/plugin/croppie.css">

@php 
  $role = Auth::user()->id_role;
@endphp

<div class="content">
  <div class="container-fluid pt-5">

    @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2)
      <a href="#" class="btn btn-xs btn-primary mb-5" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah Kontak</a>
    @endif

    <div class="row">

      @foreach($kontaks as $kontak)

      <div class="col-md-4">
        <div class="card card-profile">
          <div class="card-avatar">
            <a href="javascript:;">
              <img class="img" src="{{ asset('images') }}/kontak/{{$kontak->foto}}" />
            </a>
          </div>
          <div class="card-body">
            <div class="mb-2">
              <h6 class="card-category text-gray">Tim English</h6>
              <h4 class="card-title">{{$kontak->title}}. {{$kontak->nama}}</h4>
              <a href="https://wa.me/62{{$kontak->hp}}"><h5 class="card-title"><i class="material-icons">phone_enabled</i>{{$kontak->hp}}</h5></a>
            </div>
          </div>

          @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2)

          <div class="card-footer justify-content-center">
            <a href="#" id="btn_edit" class="btn btn-warning btn-round" data-toggle="modal" data-target="#modal_edit" onclick="set_edit('{{$kontak->id}}')"><i class="fa fa-edit mr-1"></i>Ubah</a>
            <a href="#" id="btn_delete" class="btn btn-danger btn-round"  onclick="confirmDel('{{$kontak->id}}', '{{$kontak->nama}}')"><i class="fa fa-trash mr-1"></i>Hapus</a>
          </div>

          {{ Form::open(array('url' => '/delete_kontak', 'id' => 'delForm'.$kontak->id)) }}
          @csrf
          {{ Form::hidden('id', $kontak->id) }}
          {{ Form::close() }}

          @endif

        </div>
      </div>

      @endforeach


    </div>

  </div>
</div>

@if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2)

@include('kontak.modal_add_kontak')
@include('kontak.modal_edit_kontak')

@endif

<script>

function confirmDel(id, name) {
    var txt;
    var r = confirm("Yakin akan menghapus kontak? \n\nNama: " + name);
    if (r == true) {
        txt = "You pressed OK!"; 
        $('#delForm'+id).submit();
    } else {
        txt = "You pressed Cancel!";
    }
}

</script>

@endsection
