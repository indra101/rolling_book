

<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ubah Kontak</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/edit_kontak','files'=>'true')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama</label>
                          {{ Form::text('nama_edit', '', array('class' => 'form-control pl-2', 'id' => 'nama_edit', 'required' => 'required')) }}
  
                          @if ($errors->has('nama_edit'))
                            <span class="help-block text-danger">
                                <small>Nama belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Title</label>
                          {{ Form::select('title_edit', array('' => 'Pilih Title', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms'), 0, array('class' => 'form-control pl-2', 'id' => 'title_edit', 'required' => 'required')) }}
  
                          @if ($errors->has('title_edit'))
                            <span class="help-block text-danger">
                                <small>Title belum dipilih</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nomor HP</label>
                          {{ Form::number('hp_edit', '', array('class' => 'form-control pl-2', 'id' => 'hp_edit', 'required' => 'required')) }}
  
                          @if ($errors->has('hp_edit'))
                            <span class="help-block text-danger">
                                <small>Nomor HP belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                    <div class="col-md-12">
                      <div>
                        <label class="bmd-label-floating">Upload File</label>
                        <div>

                          <div id="upload-demo" style="width:350px"></div>
                          <input type="file" id="upload" style="display: none;">
                            <a href="#" class="btn btn-warning btn-round" id="tombolUpload">Upload Foto</a>
                            
                            <input type="hidden" id="images" name="image">
                          
                          {{-- @if ($errors->has('file_upload_edit'))
                            <span class="help-block text-danger">
                                <small>{{ $errors->first('file_upload_edit') }}</small>
                            </span>
                          @endif
                          <div class="mt-4">
                            <a href="#" class="btn btn-success" id="btn_upload_edit"><i class="material-icons mr-2">insert_photo</i>Upload File</a>
                            <input type="file" name="file_upload_edit" placeholder="Choose image" id="image_edit" style="display: none;" >
                          </div>
                          <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                          <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div> --}}

                        </div>
                      </div>
                    </div>
                </div>
  
                    <input type="hidden" name="id_kontak" id="id_kontak">
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanEdit" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanEdit" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanEdit').click(function() {

  if(confirm('Data sudah benar?') ){
      $('#btnSimpanEdit').hide()
      $('#btnLoadSimpanEdit').show()

      $uploadCrop.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (resp) {
        //alert(resp)
        $('#images').val(resp)
        //alert($('#images').val())
      });

      return true;
  } else {
      return false;
  }
});

function set_edit(id) {
  $.ajax({
      url: '{{ route("get_kontak_edit") }}',
      data: 'id=' + id,
      type: "GET",
      dataType: "json",
      success: function(data) {
        $('#id_kontak').val(data.id);
        $('#nama_edit').val(data.nama);
        $('#nama_edit').trigger("change");
        $('#title_edit').val(data.title);
        $('#hp_edit').val(data.hp);
        $('#hp_edit').trigger("change");
        //$('#image_preview_container_edit').attr('src', "{{ asset('images/kontak') }}/" + data.foto);
        alert('Data kontak akan diubah')
        $uploadCrop.croppie('bind', {
          url: "{{ asset('images/kontak') }}/" + data.foto
        }).then(function(resp){
          console.log('jQuery bind complete');
        });
      }
  });
  
}

$('#btn_upload_edit').click(function() {
  $('#image_edit').click();
});

$('#image_preview_container_edit').click(function() {
  $('#image_edit').click();
});

$('#image_edit').change(function(e){

var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
var name = this.files[0].name;
var name_arr = name.split(".");
var type = name_arr[name_arr.length - 1];
var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
var cek_size = size >= 10; // max file size 10 MB
var cek_tipe = allowed.indexOf(type) == -1;

if(cek_size || cek_tipe) {
    var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
    if(cek_size)
        pesan += pesan_size;

    if(cek_tipe)
        pesan += pesan_tipe;

    alert(pesan);
    this.value = '';

} else {
    
    let reader = new FileReader();
    reader.onload = (e) => { 
        $('#image_preview_container_edit').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 

}

});


$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

$('#tombolUpload').click(function() {
  $('#upload').click();
});

$uploadCrop = $('#upload-demo').croppie({
  //url: "{{ asset('images/profil/'. (empty($user->foto) ? 'no-image.png' : $user->foto)) }}",
    enableExif: true,
    viewport: {
        width: 250,
        height: 250,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 250
    }
});

var isupload = false;

$('#upload').on('change', function () { 
	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    }
    reader.readAsDataURL(this.files[0]);

    isupload = true;
});
  
</script>