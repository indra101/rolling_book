<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Kontak</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/add_kontak','files'=>'true')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama</label>
                          {{ Form::text('nama', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}
  
                          @if ($errors->has('nama'))
                            <span class="help-block text-danger">
                                <small>Nama belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Title</label>
                        {{ Form::select('title', array('' => 'Pilih Title', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Ms' => 'Ms'), 0, array('class' => 'form-control pl-2', 'id' => 'title', 'required' => 'required')) }}

                        @if ($errors->has('title'))
                          <span class="help-block text-danger">
                              <small>Title belum diisi</small>
                          </span>
                        @endif
                      </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Nomor HP</label>
                      {{ Form::number('hp', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}

                      @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <small>Nomor HP belum diisi</small>
                        </span>
                      @endif
                    </div>
                  </div>
              </div>

              <div class="row mt-4">
                <div class="col-md-12">
                  <div>
                    <label class="bmd-label-floating">Upload Foto</label>
                    <div>

                      <div id="upload-add" style="width:350px"></div>
                      <input type="file" id="upload_add" style="display: none;">
                        <a href="#" class="btn btn-warning btn-round" id="tombolUpload_add">Upload Foto</a>
                        
                        <input type="hidden" id="images_add" name="image">
                    
                      {{-- <div>
                        <img id="image_preview_container" class="img-fluid" src=""  style="margin: auto; max-height: 400px; border-radius: 5px;">
                      </div>
                      @if ($errors->has('file_upload'))
                        <span class="help-block text-danger">
                            <small>{{ $errors->first('file_upload') }}</small>
                        </span>
                      @endif
                      <div class="mt-4">
                          <a href="#" class="btn btn-success" id="btn_upload"><i class="material-icons mr-2">insert_photo</i>Upload File</a>
                          <input type="file" name="file_upload" placeholder="Choose image" id="image" style="display: none;" {{ (empty($riwayat)) ? 'required' : '' }}>
                      </div>
                      <div><span style="font-size: 13px; color: red"><i>Format file: .jpg, .jpeg, .png</i></span></div>
                      <div><span style="font-size: 13px; color: red"><i>Maksimum ukuran file: 10 MB</i></span></div> --}}
    
    
                    </div>
                  </div>
                </div>
                
              </div>
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
    $('#btnSimpanAdd').hide()
    $('#btnLoadSimpanAdd').show()

    $uploadCrop_add.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function (resp) {
      //alert(resp)
      $('#images_add').val(resp)
      //alert($('#images').val())
    });
    
    return true;
  } else {
    return false;
  }

});

$('#btn_upload').click(function() {
  $('#image').click();
});

$('#image_preview_container').click(function() {
  $('#image').click();
});

$('#image').change(function(e){

var size = Math.round(((this.files[0].size/1024/1024) + 0.00001) * 100) / 100;
var name = this.files[0].name;
var name_arr = name.split(".");
var type = name_arr[name_arr.length - 1];
var allowed = ["jpg", "jpeg", "png", "JPG", "JPEG", "PNG"];
var pesan_size = "\nMohon masukkan file dengan ukuran max. 10 MB";
var pesan_tipe = "\nMohon masukkan file dengan format yang diperbolehkan";
var cek_size = size >= 10; // max file size 10 MB
var cek_tipe = allowed.indexOf(type) == -1;

if(cek_size || cek_tipe) {
    var pesan = "Nama File: " + name + "\nUkuran File: " + size + " MB\n";
    if(cek_size)
        pesan += pesan_size;

    if(cek_tipe)
        pesan += pesan_tipe;

    alert(pesan);
    this.value = '';

} else {
    
    let reader = new FileReader();
    reader.onload = (e) => { 
        $('#image_preview_container').attr('src', e.target.result); 
    }
    reader.readAsDataURL(this.files[0]); 

}

});

$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$('#tombolUpload_add').click(function() {
  $('#upload_add').click();
});

$uploadCrop_add = $('#upload-add').croppie({
  //url: "{{ asset('images/profil/'. (empty($user->foto) ? 'no-image.png' : $user->foto)) }}",
    enableExif: true,
    viewport: {
        width: 250,
        height: 250,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 250
    }
});

var isupload = false;

$('#upload_add').on('change', function () { 
	var reader = new FileReader();
    reader.onload = function (e) {
    	$uploadCrop_add.croppie('bind', {
    		url: e.target.result
    	}).then(function(){
    		console.log('jQuery bind complete');
    	});
    }
    reader.readAsDataURL(this.files[0]);

    isupload = true;
});

</script>