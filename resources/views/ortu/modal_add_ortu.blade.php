<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Orang Tua Siswa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/add_ortu')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama Orang Tua Siswa</label>
                          {{ Form::text('nama', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}
  
                          @if ($errors->has('nama'))
                            <span class="help-block text-danger">
                                <small>Nama OTS belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Email</label>
                        {{ Form::text('email', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}

                        @if ($errors->has('email'))
                          <span class="help-block text-danger">
                              <small>Email belum diisi</small>
                          </span>
                        @endif
                      </div>
                    </div>
                </div>
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
    $('#btnSimpanAdd').hide()
    $('#btnLoadSimpanAdd').show()
    return true;
  } else {
    return false;
  }

});

</script>