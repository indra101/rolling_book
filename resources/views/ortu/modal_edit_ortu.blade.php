<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ubah Data OTS</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/edit_ortu')) }}
            @csrf
        
            <div class="modal-body">
  
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Nama OTS</label>
                      {{ Form::text('nama_edit', '', array('class' => 'form-control pl-2', 'id' => 'nama_edit', 'required' => 'required')) }}

                      @if ($errors->has('nama_edit'))
                        <span class="help-block text-danger">
                            <small>Nama OTS belum diisi</small>
                        </span>
                      @endif
                    </div>
                  </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email</label>
                    {{ Form::text('email_edit', '', array('class' => 'form-control pl-2', 'id' => 'email_edit', 'required' => 'required')) }}

                    @if ($errors->has('nama_edit'))
                      <span class="help-block text-danger">
                          <small>Email belum diisi</small>
                      </span>
                    @endif
                  </div>
                </div>
            </div>

            </div>

            <input type="hidden" name="id_ortu" id="id_ortu">
                
            <div class="modal-footer">
                <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                <input id="btnSimpanEdit" class="btn btn-primary" type="submit" value="Simpan"/>
                <button id="btnLoadSimpanEdit" class="btn btn-primary" type="button" style="display: none;" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
            </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanEdit').click(function() {

  if(confirm('Data sudah benar?') ){
    //$('#btnSimpanEdit').hide()
    //$('#btnLoadSimpanEdit').show()
    return true;
  } else {
    return false;
  }

});

function set_edit(id) {
  $.ajax({
      url: '{{ route("get_ortu_edit") }}',
      data: 'id=' + id,
      type: "GET",
      dataType: "json",
      success: function(data) {
        $('#id_ortu').val(data.id);
        $('#nama_edit').val(data.nama);
        $('#nama_edit').trigger("change");
        $('#email_edit').val(data.email);
        $('#email_edit').trigger("change");
      }
  });
}

</script>