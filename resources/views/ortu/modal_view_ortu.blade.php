<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Data OTS</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
        
            <div class="modal-body">
  
              <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="bmd-label-floating">Nama OTS</label>
                      {{ Form::text('nama_edit', '', array('class' => 'form-control pl-2', 'id' => 'nama_view', 'disabled')) }}
                    </div>
                  </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Email</label>
                    {{ Form::text('email_edit', '', array('class' => 'form-control pl-2', 'id' => 'email_view', 'disabled')) }}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Nomor HP</label>
                    {{ Form::text('hp', '', array('class' => 'form-control pl-2', 'id' => 'hp_view', 'disabled')) }}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="bmd-label-floating">Alamat</label>
                    {{ Form::text('alamat', '', array('class' => 'form-control pl-2', 'id' => 'alamat_view', 'disabled')) }}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <img class="img-fluid rounded-circle shadow" id="foto" src="" style="border-radius: 10px;"/>
                </div>
              </div>

            </div>
                
            <div class="modal-footer">
                <input class="btn btn-primary btn-danger" type="button" value="Tutup" data-dismiss="modal"/>
            </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

function set_view(id) {
  $.ajax({
      url: '{{ route("get_ortu_view") }}',
      data: 'id=' + id,
      type: "GET",
      dataType: "json",
      success: function(data) {
        $('#id_ortu').val(data.id);
        $('#nama_view').val(data.name);
        $('#nama_view').trigger("change");
        $('#email_view').val(data.email);
        $('#email_view').trigger("change");
        $('#hp_view').val(data.hp);
        $('#hp_view').trigger("change");
        $('#alamat_view').val(data.alamat);
        $('#alamat_view').trigger("change");
        if(!data.foto) {
          data.foto = 'no-image.png';
        }
        $('#foto').attr('src', "{{ asset('images/profil/')}}/" + data.foto);
      }
  });
}

</script>