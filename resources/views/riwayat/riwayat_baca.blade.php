@extends('layouts.app')

@section('content')

<style>

  .label-home {
    background-color: #3490dc;
    padding: 10px;
    border-radius: 10px;
    /*margin-right: 15px;*/
    font-weight: 500;
    color: white;
  }
  
  .label-home2 {
    background-color: orangered;
    padding: 10px;
    border-radius: 10px;
    /*margin-right: 15px;*/
    font-weight: 500;
    color: white;
  }
  
  </style>

<script src="{{ asset('js') }}/jQueryRotate.js"></script>

@php 
  $role = Auth::user()->id_role;
@endphp

<div class="content">
  <div class="container-fluid">

      <div style="float: right; margin-bottom: 40px;">
        <span style="font-weight: bold">Tahun Ajaran</span><span class="label-home ml-2 mr-2">{{$tahun_ajar->tahun}}</span><span style="font-weight: bold">Semester</span><span class="label-home2 ml-2 mr-2">{{$tahun_ajar->semester}}</span>
      </div>

    <div class="main-card mb-3 card">
      
      <div class="card-header card-header-warning">
        {{-- <h3 class="card-title">Riwayat Baca</h3> --}}
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
              <ul class="nav nav-tabs" data-tabs="tabs">

                @if(Auth::user()->id_role == 3)

                @php $i = 0; @endphp
                @foreach ($siswas as $siswa)
                
                <li class="nav-item">
                  <a class="nav-link @if($i++ == 0) active @else @endif" href="#profile" data-toggle="tab" onclick="ubah_siswa({{$siswa->id}}, '{{$siswa->nama}}')">
                    <i class="material-icons">face</i> {{$siswa->nama}}
                    <div class="ripple-container"></div>
                  </a>
                </li>

                @endforeach

                @else
                  <h3 class="card-title">Daftar Riwayat Baca Siswa</h3>
                @endif
                
              </ul>
            </div>
          </div>
      </div>

        <div class="card-body">

        @if(Auth::user()->id_role == 3)
        <h3 id="nama_anak">{{ $siswas[0]->nama }}</h3>

        @if($report) 
        <a href="/laporan/{{ $siswas[0]->id }}" id="btnAdd" class="btn btn-xs btn-primary"><i class="fa fa-plus-circle mr-2"></i> Tambah Baca Buku</a>
        @endif
        {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
        <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
        <br/><br/>
        @endif
        <div class="overflow-auto">
            <table class="table table-striped table-hover" id="riwayats-table" style="background-color: white;">
                <thead>
                    <tr>
                        <th style="max-width: 70px;"><strong>No</strong></th>
                        @if($role != 3)
                        <th style="min-width: 200px;"><strong>Nama Siswa</strong></th>
                        <th style="min-width: 150px;"><strong>Kelas</strong></th>
                        @endif
                        <th style="min-width: 150px;"><strong>Tanggal Baca</strong></th>
                        <th style="min-width: 150px;"><strong>Judul Buku</strong></th>
                        <th style="min-width: 150px;"><strong>Written By</strong></th>
                        <th><strong>Foto Book Review</strong></th>
                        <th style="min-width: 150px;"><strong></strong></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

  </div>
</div>

</div>

<div class="modal fade overflow-auto" id="modal_foto" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
  <div class="modal-dialog" style="max-width: 900px">
      <div class="modal-content">

          <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel">Foto Book Review</h4>
              <a href="#" id="btn_rotate" class="close"><i class="material-icons" style="font-size: xx-large;">rotate_right</i></a>  
              <button type="button" class="close" data-dismiss="modal" style="margin-left: 0;">&times;</button>
          </div>

          <div class="modal-body" style="margin: auto;">
            <img src="" id="foto" class="book-img rounded shadow img-fluid" alt="slide" style="max-height: 800px;">
          </div>

          <div class="modal-footer" style="margin: auto;">
            
          </div>
      </div>
  </div>
</div>

    <script>
    var table;
    var rotate = 0;

    $(function() {

        table = $('#riwayats-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{!! route('get_riwayats') !!}?id_siswa={{ (Auth::user()->id_role == 3) ? $siswas[0]->id : '' }}",
                data: {
                    "id_sub_level": "",
                },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                @if($role != 3)
                { data: 'nama_siswa', name: 'nama_siswa' , searchable: true},
                { data: null, name: 'kelas', render: function ( data, type, row ) {
                    return data.nama_level + ' - ' + data.nama_sub_level;
                } },  
                @endif
                { data: 'tgl_baca', name: 'tgl_baca' , searchable: true},
                { data: 'judul', name: 'judul' , searchable: true},
                { data: 'written_by', name: 'written_by' , searchable: true},
                { data: null, name: 'file', render: function ( data, type, row ) {
                    return '<a href="#" data-toggle="modal" data-target="#modal_foto" onclick="set_foto(' + data.id_siswa + ', \'' + data.file + '\')"><img src="{{ asset('images/laporan') }}/' + data.id_siswa + '/thumb/' + data.file + '" class="book-img rounded shadow" alt="slide" style="max-height: 100px;"></a>';
                } },  
                { data: null, name: 'action', render: function ( data, type, row ) {
                    return '<a href="/edit_riwayat_page/' + data.id + '" title="Ubah" style="padding: 10px;"><i class="material-icons">create</i></a>' +  
                    '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick="confirmDel(' + data.id + ')" title="Hapus"><i class="material-icons">delete</i></a>' +
                    '<form class="delete" action="{{route('delete_riwayat')}}" method="post">@csrf' + 
                    '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' +                 
                    '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                    '</form></div>';
                } },
            ],
            order: [[3, "desc"]],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });

        @if(!empty($errors->all()))
          $('#modal_add').modal('toggle');
        @endif
        
    });

    function set_foto(id_siswa, file) {
      rotate = 0;
      $("#foto").rotate(0);
      $('#foto').attr('src', "{{ asset('images/laporan') }}/" + id_siswa + '/' + file);
    }

    function ubah_siswa(id, nama) {
        $('#nama_anak').html(nama);
        $('#btnAdd').attr('href', '/laporan/' + id)
        table.ajax.url("{!! route('get_riwayats') !!}" + "?id_siswa=" + id);
        table.ajax.reload();
    }

    function confirmDel(id) {
        var txt;
        var r = confirm("Yakin akan menghapus data?");
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function get_sub_level() {
      var id_level = $('#id_level').val();

      $.ajax({
          url: '{{ route("get_sub_levels") }}',
          data: 'id_level=' + id_level,
          type: "GET",
          dataType: "json",
          success: function(data) {
              $('#id_sub_level').empty();
              $('#id_sub_level').append('<option value="">Pilih Sub Level</option>');
              $.each(data, function(key, value) {
                  $('#id_sub_level').append('<option value="'+ key +'">'+ value +'</option>');
              });
          }
      });
    }

    $('#btn_rotate').click(function() {
      rotate += 90
      $("#foto").rotate({ animateTo:rotate});
    });

    </script>
    @stack('scripts')

@endsection
