<div class="modal fade" id="modal_tahun" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ganti Tahun Ajaran</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{-- <form class="form-horizontal" method="POST" action="{{ route('add_buku') }}"> --}}
            {{ Form::open(array('url' => '/set_tahun_ajar')) }}
  
            {{ csrf_field() }}
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Tahun Ajaran</label>
                          {{ Form::select('tahun_ajar', $tahuns, 0, array('class' => 'form-control pl-2', 'id' => 'tahun_ajar')) }}
  
                          @if ($errors->has('id_level'))
                            <span class="help-block text-danger">
                                <small>Tahun Ajaran belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Semester</label>
                          {{ Form::select('semester', array('1' => '1', '2' => '2'), 0, array('class' => 'form-control pl-2', 'id' => 'semester')) }}
  
                          @if ($errors->has('id_sub_level'))
                            <span class="help-block text-danger">
                                <small>Semester belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>
  
                    <input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id }}">
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
      $('#btnSimpanAdd').hide()
      $('#btnLoadSimpanAdd').show()
      return true;
  } else {
      return false;
  }
});

</script>