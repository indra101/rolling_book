<div class="modal fade" id="modal_report" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Enable/Disable Report</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
  
            {{ Form::open(array('url' => '/set_report')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          Apakah anda yakin akan mengubah status Report?
                        </div>
                      </div>
                  </div>
  
                </div>

                <input type="hidden" id="val" name="val">
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            {{ Form::close() }}
            
        </div>
    </div>
  </div>

<script>





</script>