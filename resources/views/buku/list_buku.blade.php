@extends('layouts.app')

@section('content')


<div class="content">
  <div class="container-fluid">

    <div class="main-card mb-3 card">
      
      <div class="card-header card-header-info">
        <h3 class="card-title">Daftar Buku</h3>
      </div>

        <div class="card-body">

        <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_add"><i class="fa fa-plus-circle mr-2"></i> Tambah Buku</a>
        {{-- <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modal_upload"><i class="fa fa-save mr-2"></i> Upload Files</a> --}}

        {{-- <a href="{{route('get.excelItem')}}" class="btn btn-xs btn-success"><i class="fa fa-download"></i> Export to Excel</a>
        <a href="{{route('get.jsonItem')}}" class="btn btn-xs btn-secondary"><i class="fa fa-download"></i> Export to JSON</a> --}}
        <br/><br/>
        <div class="overflow-auto">
            <table class="table table-striped table-hover" id="bukus-table" style="background-color: white;">
                <thead>
                    <tr>
                        <th style="max-width: 70px;"><strong>No</strong></th>
                        <th style="min-width: 150px;"><strong>Judul</strong></th>
                        <th style="min-width: 150px;"><strong>Kelas</strong></th>
                        <th style="min-width: 150px;"><strong>Tgl Dibuat</strong></th>
                        <th><strong>Cover</strong></th>
                        <th style="min-width: 150px;"><strong>Status</strong></th>
                        <th style="min-width: 150px;"><strong></strong></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

  </div>
</div>

@include('buku.modal_add_buku')
@include('buku.modal_edit_buku')
@include('buku.modal_upload')

    <script>
    $(function() {

        var table = $('#bukus-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{!! route('get_bukus') !!}",
                data: {
                    "id_sub_level": ""
                },
            },
            columns: [
                { data: 'no', name: 'no' , searchable: true},
                { data: 'judul', name: 'judul' , searchable: true},
                { data: null, name: 'level', render: function ( data, type, row ) {
                    return data.nama_level + ' - ' + data.nama_sub_level;
                } },
                { data: 'created_at', name: 'created_at' , searchable: true},
                { data: null, name: 'cover', render: function ( data, type, row ) {
                    return '<img src="{{ asset('buku/') }}/' + data.id + '/thumb/page-1.jpg" class="book-img rounded shadow" alt="slide" style="max-height: 100px;">';
                } },  
                { data: null, name: 'status', render: function ( data, type, row ) {
                    var status_txt = (data.aktif == 1) ? 'Aktif' : 'Tidak Aktif';
                    var is_aktif = (data.aktif == 1) ? 'checked' : '';

                    return '<div class="custom-control custom-switch ml-3" style="display: inline-block">' +
                    '<input type="checkbox" class="custom-control-input" id="customSwitch' + data.no + '" ' + is_aktif + ' onclick="set_status(\'' + data.id + '\')">' + 
                    '<label class="custom-control-label" for="customSwitch' + data.no + '">' + status_txt + '</label>' +
                    '</div>';
                } },
                { data: null, name: 'action', render: function ( data, type, row ) {
                  return '<a href="/read/' + data.id + '?from=list" title="Lihat" style="padding: 10px;"><i class="material-icons">search</i></a>' +
                    '<a href="#" title="Ubah" data-toggle="modal" data-target="#modal_edit" style="padding: 10px;" onclick="set_edit(' + data.id + ')"><i class="material-icons">create</i></a>' +  
                    '<a href="#delete-' + data.id + '" style="margin-left: 5px; padding: 10px;" onclick=\'confirmDel(' + data.id + ',"' + data.judul + '")\' title="Hapus"><i class="material-icons">delete</i></a>' +
                    '<form class="delete" action="/delete_buku" method="post">@csrf' + 
                    '<input type="hidden" class="form-control" name="id" value="' + data.id + '">' + 
                    '<input id="delButton' + data.id + '" type="submit" value="Delete" style="display: none;">' + 
                    '</form></div>';
                } },
            ],
            order: [[3, "desc"]],
            columnDefs: [{
                "defaultContent": "-",
                "searchable": false,
                "orderable": false,
                "targets": 0
              }]
        });

        @if(!empty($errors->all()))
          $('#modal_add').modal('toggle');
        @endif
        
    });

    function set_status(id) {
        window.location.href = '{{route("set_status_buku")}}?id=' + id
        // alert(id)
        // $('#link' + id).click();
    }

    function confirmDel(id, name) {
        var txt;
        var r = confirm("Yakin akan menghapus buku? \n\nJudul: " + name);
        if (r == true) {
            txt = "You pressed OK!"; 
            $('#delButton'+id).click();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    </script>
    @stack('scripts')

    
</div>
@endsection
