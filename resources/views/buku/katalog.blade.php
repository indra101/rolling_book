@extends('layouts.app')

@section('content')

<style>

.book-img {
  transition-duration: .1s;
  max-height: 200px;
}

.book-img:hover {
  border: solid 4px #FC5185;
}

.teks {
  font-size: large;
}

@media only screen and (max-width: 700px){
  .book-img {
    max-height: 140px;
  }

  .teks {
    font-size: small;
  }
}

</style>

<div class="content">
  <div class="container-fluid">
    
    @foreach ($sub_levels as $sub_lvl)

    <div class="card card-stats">
      <div class="card-header card-header-warning card-header-icon" style="text-align: left;">
        <div class="card-icon">
          <h2><strong>{{ $sub_lvl->level->nama }}</strong> - {{ $sub_lvl->nama }}</h2>
        </div>
        <h3 class="card-title float-left">
          <div class="row ml-2 mr-2 overflow-auto">

           
            @if($sub_lvl->bukus->isEmpty())

            <div class="mr-4" style="height: 120px;">
              <div class="d-flex justify-content-center ">
                <h5>Belum ada buku</h5>
              </div>
            </div>

            @else
              @foreach ($sub_lvl->bukus as $buku)
                  
              @if($buku->aktif)
              <div class="mr-4">
                <div class="d-flex justify-content-center ">
                  <a href="/read/{{ $buku->id }}"> <img src="{{ asset('buku/'.$buku->id) }}/thumb/page-1.jpg" class="book-img rounded shadow" alt="slide" style=""></a>
                </div>
                <div class="teks d-flex justify-content-center mt-2">{{ $buku->judul }}</div>
              </div>
              @endif

              @endforeach

            @endif

          </div>
        </h3>
      </div>
      
    </div>

    @endforeach


  </div>
</div>

@endsection
