<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('material-dashboard') }}/assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
            <img src="{{ asset('images') }}/rb-text_small.jpg" class="img-fluid mx-auto d-block"/>
            <img src="{{ asset('images') }}/logo_small.png" style="max-height: 50px; "/>
        </a>
    </div>

    @php 
      $role = Auth::user()->id_role;
      if(empty($menu)) $menu = 'home';
    @endphp

    <div class="sidebar-wrapper" style="padding-bottom: 150px;">
      <ul class="nav">
        <li class="nav-item @if($menu == 'home') active @endif">
          <a class="nav-link" href="{{route('home')}}">
            <i class="material-icons">dashboard</i>
            <p>Beranda</p>
          </a>
        </li>
        {{-- <li class="nav-item @if($menu == 'read') active @endif">
          <a class="nav-link" href="/read/30">
            <i class="material-icons">chrome_reader_mode</i>
            <p>Baca Buku</p>
          </a>
        </li> --}}
        {{-- <li class="nav-item @if($menu == 'katalog') active @endif">
          <a class="nav-link" href="{{route('katalog')}}">
            <i class="material-icons">content_paste</i>
            <p>Katalog</p>
          </a>
        </li> --}}
        <li class="nav-item dropdown @if($menu == 'katalog') active @endif">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 14px;">
            <i class="material-icons">menu_book</i>
            Daftar Buku
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @foreach($level_sidebar as $lvl)
              <a class="dropdown-item" href="/katalog_level/{{ $lvl->id }}">{{ $lvl->nama }}</a>
            @endforeach
          </div>
        </li>
        <li class="nav-item @if($menu == 'riwayat_baca') active @endif">
          <a class="nav-link" href="{{route('riwayat_baca')}}">
            <i class="material-icons">bubble_chart</i>
            <p>Riwayat Baca</p>
          </a>
        </li>

        @if($role == 1 || $role == 2)

        <li class="nav-item @if($menu == 'list_buku') active @endif">
          <a class="nav-link" href="{{route('list_book')}}">
            <i class="material-icons">library_books</i>
            <p>Pengaturan Buku</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'level') active @endif">
          <a class="nav-link" href="{{route('level')}}">
            <i class="material-icons">class</i>
            <p>Pengaturan Kelas</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'siswa') active @endif">
          <a class="nav-link" href="{{route('siswa')}}">
            <i class="material-icons">face</i>
            <p>Data Siswa</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'ortu') active @endif">
          <a class="nav-link" href="{{route('ortu')}}">
            <i class="material-icons">supervisor_account</i>
            <p>Data Orang Tua</p>
          </a>
        </li>
        <li class="nav-item @if($menu == 'report') active @endif">
          <a class="nav-link" href="{{route('report')}}?id_level=3">
            <i class="material-icons">assessment</i>
            <p>Report Kelas</p>
          </a>
        </li>

        @endif

        <li class="nav-item @if($menu == 'kontak') active @endif">
          <a class="nav-link" href="{{route('kontak')}}">
            <i class="material-icons">contacts</i>
            <p>Kontak Tim English</p>
          </a>
        </li>
        
        {{-- <li class="nav-item ">
          <a class="nav-link" href="./map.html">
            <i class="material-icons">location_ons</i>
            <p>Maps</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./notifications.html">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./rtl.html">
            <i class="material-icons">language</i>
            <p>RTL Support</p>
          </a>
        </li>
        <li class="nav-item active-pro ">
          <a class="nav-link" href="./upgrade.html">
            <i class="material-icons">unarchive</i>
            <p>Upgrade to PRO</p>
          </a>
        </li> --}}
      </ul>
    </div>
  </div>