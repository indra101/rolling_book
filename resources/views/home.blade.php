@extends('layouts.app')

@section('content')

<style>

.label-home {
  background-color: #3490dc;
  padding: 10px;
  border-radius: 10px;
  /*margin-right: 15px;*/
  font-weight: 500;
  color: white;
}

.label-home2 {
  background-color: orangered;
  padding: 10px;
  border-radius: 10px;
  /*margin-right: 15px;*/
  font-weight: 500;
  color: white;
}

</style>

<div class="content">
    <div class="container-fluid">

        <div class="row">

        <div class="card card-stats">
            <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <h1>Rolling Book</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <img src="{{ asset('images') }}/kids1.jpg" class="img-fluid mx-auto d-block" style="max-height: 600px;"/>
                  </div>
                  <div class="col-md-4">

                    @if(Auth::user()->id_role == 1 || Auth::user()->id_role == 2)

                    {{-- <h4><strong>Tahun Ajaran</strong></h4> --}}
                    <h3><strong>Tahun Ajaran</strong></h3>
                    <h3><span class="label-home"><strong>{{$tahun_ajar->tahun}}</strong></span></h3>
                    <h3><strong>Semester</strong></span></h3>
                    <h3><span class="label-home2"><strong>{{$tahun_ajar->semester}}</strong></span></h3>
                    <a href="#" class="btn btn-xs btn-primary mb-5" data-toggle="modal" data-target="#modal_tahun"><i class="fa fa-calendar mr-2"></i> Ganti Tahun Ajaran</a>

                      {{ Form::open(array('url' => '/edit_kontak','files'=>'true')) }}
                      @csrf

                      <input type="hidden" name="val" id="val">

                      @if($report) 
                        <h3><strong>Report Enabled</strong></h3>
                        <a href="#" class="btn btn-xs btn-danger mb-5" data-toggle="modal" data-target="#modal_report" id="btn_report" onclick="set_report(0)"><i class="fa fa-times-circle mr-2"></i> Disable Report</a>
                      @else
                        <h3><strong>Report Disabled</strong></h3>
                        <a href="#" class="btn btn-xs btn-success mb-5" data-toggle="modal" data-target="#modal_report" id="btn_report" onclick="set_report(1)"><i class="fa fa-check-circle mr-2"></i> Enable Report</a>
                      @endif
                      
                    @endif

                    @if(Auth::user()->id_role == 3)

                    <div style="float: right; margin-bottom: 40px;">
                      <span style="font-weight: bold">Tahun Ajaran</span><span class="label-home ml-2 mr-2">{{$tahun_ajar->tahun}}</span><span style="font-weight: bold">Semester</span><span class="label-home2 ml-2 mr-2">{{$tahun_ajar->semester}}</span>
                    </div>

                    @php $n = 1; @endphp

                    @foreach ($siswas as $siswa)

                      @php 
                        if($n == 1) $tipe = 'success'; 
                        else if($n == 2) $tipe = 'warning';
                        else if($n == 3) $tipe = 'info'; 
                        else $tipe = 'primary'; 
                      @endphp

                      <div class="card card-chart shadow">
                        <div class="card-header card-header-{{$tipe}}">
                          <div>
                            <h3><strong>{{$siswa->nama}}</strong></h3>
                          </div>
                          <div><h4>{{$siswa->nama_level}} {{$siswa->nama_sub_level}}</h4></div>
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Sudah Baca</h4>
                          <p class="card-category">
                            <h2>{{$siswa->jml_baca}} Buku</h2>
                          </p>
                        </div>
                        {{-- <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons">access_time</i> updated 4 minutes ago
                          </div>
                        </div> --}}
                      </div>

                      @php 
                        if($n > 3) $n = 1;
                        else $n++;
                      @endphp
                          
                    @endforeach

                    @endif

                  </div>
                </div>
            </div>
          </div>
        
        
        </div>

    </div>
  </div>
@endsection

@include('modal_report')
@include('modal_tahun')

<script>

function set_report(val) {
  $('#val').val(val);
}

</script>