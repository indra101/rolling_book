@php 
    $type = '';

    if($message = Session::get('success')) {
        $type = 'success';
    } else if($message = Session::get('warning')) {
        $type = 'warning';
    } else if($message = Session::get('danger')) {
        $type = 'danger';
    } else if($message = Session::get('primary')) {
        $type = 'primary';
    } else if($message = Session::get('info')) {
        $type = 'info';
    }
@endphp

<script>

function showNotification(from, align){
    $.notify({
        icon: "add_alert",
        message: "{{$message}}"
    },{
        type: '{{ $type }}',
        timer: 4000,
        placement: {
            from: from,
            align: align
        }
    });
}

</script>

@if(Session::get('success') || Session::get('warning') || Session::get('danger') || Session::get('primary'))
    <script>
        $(function() {
            showNotification('top','right')
        });
    </script>
@endif