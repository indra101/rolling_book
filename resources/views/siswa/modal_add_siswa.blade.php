<div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
  
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Siswa</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
            </div>
  
            {{ Form::open(array('url' => '/add_siswa')) }}
            @csrf
        
                <div class="modal-body">
  
                  <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama Siswa</label>
                          {{ Form::text('nama', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}
  
                          @if ($errors->has('nama'))
                            <span class="help-block text-danger">
                                <small>Nama siswa belum diisi</small>
                            </span>
                          @endif
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Nama Panggilan</label>
                        {{ Form::text('panggilan', '', array('class' => 'form-control pl-2', 'required' => 'required')) }}

                        @if ($errors->has('panggilan'))
                          <span class="help-block text-danger">
                              <small>Nama panggilan belum diisi</small>
                          </span>
                        @endif
                      </div>
                    </div>
                </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Kelas</label>
                        {{ Form::select('id_level', $levels, 0, array('class' => 'form-control pl-2', 'id' => 'id_level', 'onchange' => 'get_sub_level()', 'required' => 'required')) }}

                        @if ($errors->has('id_level'))
                          <span class="help-block text-danger">
                              <small>Kelas belum dipilih</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Rombel</label>
                        {{ Form::select('id_sub_level', array('' => 'Pilih Rombel'), 0, array('class' => 'form-control pl-2', 'id' => 'id_sub_level', 'required' => 'required')) }}

                        @if ($errors->has('id_sub_level'))
                          <span class="help-block text-danger">
                              <small>Rombel belum dipilih</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="bmd-label-floating">Orang Tua</label>
                        {{ Form::select('id_ortu', $ortus, 0, array('class' => 'form-control pl-2 js-select2', 'id' => 'id_ortu', 'required' => 'required')) }}

                        @if ($errors->has('id_ortu'))
                          <span class="help-block text-danger">
                              <small>Orang Tua belum dipilih</small>
                          </span>
                        @endif
                      </div>
                    </div>
                  </div>
  
                </div>
                
                <div class="modal-footer">
                    <input class="btn btn-primary btn-danger" type="button" value="Batal" data-dismiss="modal"/>
                    <input id="btnSimpanAdd" class="btn btn-primary" type="submit" value="Simpan"/>
                    <button id="btnLoadSimpanAdd" class="btn btn-primary" type="button" style="display: none;" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        Loading...
                    </button>
                </div>
  
            </form>
            
        </div>
    </div>
  </div>

<script>

$('#btnSimpanAdd').click(function() {

  if(confirm('Data sudah benar?') ){
    $('#btnSimpanAdd').hide()
    $('#btnLoadSimpanAdd').show()
    return true;
  } else {
    return false;
  }

});

function get_sub_level() {
    var id_level = $('#id_level').val();

    $.ajax({
        url: '{{ route("get_sub_levels_buku") }}',
        data: 'id_level=' + id_level,
        type: "GET",
        dataType: "json",
        success: function(data) {
            $('#id_sub_level').empty();
            $('#id_sub_level').append('<option value="">Pilih Rombel</option>');
            $.each(data, function(key, value) {
                $('#id_sub_level').append('<option value="'+ key +'">'+ value +'</option>');
            });
        }
    });
}

</script>